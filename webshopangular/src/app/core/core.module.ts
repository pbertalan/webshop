import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterLayoutComponent } from './footer-layout/footer-layout.component';
import { NavBarLayoutComponent } from './nav-bar-layout/nav-bar-layout.component';
import { SideBarLayoutComponent } from './side-bar-layout/side-bar-layout.component';



@NgModule({
  declarations: [FooterLayoutComponent,NavBarLayoutComponent,SideBarLayoutComponent],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
