import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FooterLayoutComponent } from './core/footer-layout/footer-layout.component';
import { NavBarLayoutComponent } from './core/nav-bar-layout/nav-bar-layout.component';
import { SideBarLayoutComponent } from './core/side-bar-layout/side-bar-layout.component';
import { ProductModule } from './product/product.module';
import { IndexModule } from './index/index.module';

@NgModule({
  declarations: [
    AppComponent,
    FooterLayoutComponent,
    NavBarLayoutComponent,
    SideBarLayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ProductModule,
    IndexModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
